﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO.Ports;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace RS_Meter
{
    public partial class frmMain : Form
    {
        #region properties

        SerialPort _comPort = new SerialPort();

        public SerialPort ComPort
        {
            get { return _comPort; }
            set { _comPort = value; }
        }

        #endregion

        #region functions

        private void appendSerialData(string dataStr)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(appendSerialData), new object[] { dataStr });
                return;
            }
            
            Regex re = new Regex(@"[-+]?\b[0-9]+(\.[0-9]+)?\b");
            Match m = re.Match(dataStr);
            string tabData = dataStr;

            if (m.Success)
            {
                string funct = dataStr.Substring(0, 2);
                string val = m.Value;
                string units = dataStr.Substring(9, 4);
                tabData = string.Format("{0}\t{1}\t{2}\r\n", funct.Trim(), val.Trim(), units.Trim());
            }

            rtbReadings.AppendText(tabData);
            Console.WriteLine(tabData);
        }

        private bool ComPortExists(string comPortName)
        {
            string[] portNames = SerialPort.GetPortNames();

            return portNames.Contains(comPortName);
        }

        private void listComPorts()
        {
            comPortToolStripMenuItem.DropDownItems.Clear();

            string[] portNames = SerialPort.GetPortNames();

            foreach (string portName in portNames)
            {
                ToolStripMenuItem itm = new ToolStripMenuItem();
                itm.Name = portName + "ToolStripMenuItem";
                itm.Text = portName;
                itm.Click += comToolStripMenuItem_Click;
                if (portNames.Length == 1 || ComPort.PortName == portName)
                {
                    if (!ComPortExists(ComPort.PortName))
                    {
                        comToolStripMenuItem_Click(itm, null);
                    }
                    itm.Checked = true;
                }

                comPortToolStripMenuItem.DropDownItems.Add(itm);
            }
        }

        #endregion

        #region class constructor

        public frmMain()
        {
            InitializeComponent();
        }

        #endregion

        #region event handlers

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog(this);
        }

        private void comPort_OnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            
            string indata = sp.ReadExisting();
            appendSerialData(indata);
        }

        private void comToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem itm = (ToolStripMenuItem)sender;

            if (ComPort.IsOpen)
            {
                ComPort.Close();
                lblStatus.Text = "Disconnected";
                timer1.Enabled = false;
            }
            ComPort.PortName = itm.Text;
            lblComPort.Text = itm.Text;
            itm.Checked = true;
        }

        private void connectionToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            listComPorts();

            connectToolStripMenuItem.Text = "Connect";
            if (ComPort.IsOpen)
            {
                connectToolStripMenuItem.Text = "Disconnect";
            }
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem itm = (ToolStripMenuItem)sender;

            if (!ComPortExists(ComPort.PortName))
            {
                MessageBox.Show(this, "Please select a COM Port first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (ComPort.IsOpen)
            {
                ComPort.Close();
                lblStatus.Text = "Disconnected";
                timer1.Enabled = false;
            }
            else
            {
                ComPort.Open();
                lblStatus.Text = "Connected";
                timer1.Enabled = true;
            }
            connectionToolStripMenuItem_DropDownOpening(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ComPort.IsOpen)
            {
                ComPort.Close();
                lblStatus.Text = "Disconnected";
                timer1.Enabled = false;
            }
        }


        private void frmMain_Load(object sender, EventArgs e)
        {
            ComPort.BaudRate = 1200;
            ComPort.Parity = Parity.None;
            ComPort.DataBits = 7;
            ComPort.StopBits = StopBits.Two;
            ComPort.Handshake = Handshake.None;
            ComPort.DtrEnable = true;
            ComPort.DataReceived += comPort_OnDataReceived;
            ComPort.ReceivedBytesThreshold = 14;
        }

        private void refreshListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listComPorts();
        }

        private void saveGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Image File (*.jpg)|*.jpg";
            saveFileDialog1.Title = "Save Graph to File";
            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                pictureBox1.Image.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
            }
        }

        private void saveReadingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Text File (*.txt)|*.txt";
            saveFileDialog1.Title = "Save Results to File";
            if(saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                System.IO.File.WriteAllText(saveFileDialog1.FileName, rtbReadings.Text.Replace("\n", "\r\n"));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ComPort.IsOpen)
            {
                ComPort.Write("D");
            }
        }

        #endregion
    }
}
