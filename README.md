# Radio Shack Meter 22-168A  
## Windows Program to read meter once per second  
![Radio Shack DMM (22-168A)](Reference/180px-Rs_22_168_angle.png)  
This program was written as a replacement to the original program which was released around 1994. The original program was written for DOS.  
  
I needed to capture data from Windows 10 so I hacked together this simple program to capture the data from the meter once per second and display the data in a text box. The data may then be saved to a file by selecting Save Results from the file menu.  
  
There are a few ideas which were not implemented and I may not get to as I plan to leave this project as is for now. If you make any improvements or implement any of these ideas, please send me a pull request as I would like to know about them and perhaps incorporate them here.  
  
- When negative values are read, color code the values in the results text box. This is the reason that I used a Rich Text Box rather than a simple Text Box. I have done most of the work for this as I am parsing the string returned from the meter to get the numeric value.  
- Add a graph to plot the data over time. A Picture Box Control is on the form and there is a menu item already coded to save a graph to a JPG file.  
  
There are other things that someone may think of which could be added such as capturing Min and Max values. I would like to see what people may come up with. At the same time, I am doubtful that anyone will even use this application as the meter is now 22 years old. (Wow, does not seem like it.)  
  
I am releasing this code as I did a quick search in an attempt to find someone who has written a similar application to work in Windows but did not find one. I did find an interesting project this morning though with some awesome documentation which would have been a big help if I found it last night. The project is sigrok. Sigrok has great documentation on the Radio Shack meter at [https://sigrok.org/wiki/RadioShack_22-168](https://sigrok.org/wiki/RadioShack_22-168). They even have the protocol documented as well.  
  
I did run into some issues when attempting to connect to the meter. I had set the baud rate, parity, data bits, stop bits, and flow control properly but I was not receiving data from the meter. I finally found that I needed to set DTR to True before communication would occur.  
  
I hope this helps someone working with this meter or an other meter which utilizes the [Metex 14-byte ASCII protocol](https://sigrok.org/wiki/Multimeter_ICs#Metex_14-byte_ASCII "Metex 14-byte ASCII protocol").  
  
## Usage  
To use this application, you will need Microsoft Visual Studio 2015 or better to open the project and compile the code. Microsoft has free versions available. I have not tested compiling this on the free version but it should just work. Once it compiled (Build from the menu), the executable will be in the bin\debug folder. You may copy the executable to another location if you like or run it from the bin folder. (The application may also be run from Visual Studio.)  
  
Once the application opens, select the COM Port which is connected to the meter. If you do not see a COM Port listed then check Windows Device Manager to make certain that the port is recognized by Windows.  
![Select COM Port](Reference/SelectComPort.png)  
Next, click Connect from the Connection menu  
![Connect to COM Port](Reference/ConnectToComPort.png)  
If the meter is turned on and connected to the PC, you will start to see readings being displayed every second in the left panel.  
![Data Capture](Reference/DataCapture.png)  
To save the results to a plain text file, select File > Save Readings  
![Data Capture](Reference/SaveReadings.png)  
  
**NOTE:** The data is tab delimited but only if the data contains a numeric value. This is another item for further improvement in the future.  
  
The above describes the current functionality of the application. If there is an interest, I would hope that others will complete the missing items. I simply do not have a need for those at the moment. The application does just what I need to help me troubleshoot an issue I am having with my [TOS Tricorder](https://gitlab.com/richteel/TOSTricorder/ "TOS Tricorder Project") project.